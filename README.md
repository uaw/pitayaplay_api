# Pitaya Play API

Exercice pratique du cours 8

### Installation

Avant tous, il vous faudra installer les librairies dont aura besoin cette application web pour fonctionner.

Pour ce faire, executer la commande suivante :

```
npm install
```

### Lancer l'application

Pour lancer l'application vous devrez utiliser la commande suivante :

```
npm run start
```

Puis, vous rendre à l'adresse suivante :

```
http:/localhost:3000
```
